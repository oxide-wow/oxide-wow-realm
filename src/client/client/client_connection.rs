use async_std::net::{SocketAddr, TcpStream};
use futures::{AsyncWriteExt, AsyncReadExt};
use std::time::Duration;
use async_std::future::timeout;

pub struct ClientConnection {
    pub address: SocketAddr,
    pub tcp_stream: TcpStream,
    pub connected: bool
}

impl ClientConnection {
    pub fn new(tcp_stream: TcpStream) -> Self {
        Self {
            address: tcp_stream.peer_addr().unwrap(),
            tcp_stream,
            connected: true
        }
    }

    pub async fn send_message(&mut self, server_message: Vec<u8>) {
        // println!("server -> client: {:?}", server_message);
        let _ = self.tcp_stream.write(server_message.as_ref()).await;
    }

    pub async fn receive_message(&mut self) -> Result<Vec<u8>, ()> {
        let mut buffer = vec![0; 2800];
        match timeout(Duration::from_millis(10000), self.tcp_stream.read(&mut buffer)).await {
            Ok(s) => {
                if s.unwrap() > 0 {
                    if let Some(end) = buffer.iter().rposition(|x| *x != 0) {
                        // println!("cutting {} zeros from client buffer", buffer.len() - end - 1);
                        buffer.truncate(end + 1);
                    }
                    Ok(buffer)
                } else {
                    println!("Client disconnected: {:?}", self.address);
                    self.connected = false;
                    Err(())
                }
            }
            _ => Err(())
        }
    }
}
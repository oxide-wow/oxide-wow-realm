use crate::client::crypt::client_crypt::ClientCrypt;
use crate::client::client::client_connection::ClientConnection;
use crate::client::client::connecting_client::ConnectingClient;
use crate::message::client_message::ClientMessage;
use crate::message::client::char_enum::CharEnum;

pub struct Client {
    pub client_connection: ClientConnection,
    pub client_build: u32,
    pub client_account_name: String,
    pub client_crypt: ClientCrypt
}

impl Client {
    pub fn from_connecting_client(connecting_client: ConnectingClient) -> Result<Client, ()> {
        match ClientCrypt::from_username(connecting_client.client_account_name.clone()) {
            Ok(client_crypt) => {
                println!("{} authenticated", connecting_client.client_account_name);
                Ok(Self {
                    client_connection: connecting_client.client_connection,
                    client_build: connecting_client.client_build,
                    client_account_name: connecting_client.client_account_name,
                    client_crypt
                })
            }
            _ => Err(())
        }
    }

    pub async fn handle_incoming_messages(&mut self) {
        match self.client_connection.receive_message().await {
            Ok(mut message_raw) => {
                self.client_crypt.decrypt_6_bytes(&mut message_raw);
                let mut client_message = ClientMessage::new(message_raw);
                match client_message.signature {
                    CharEnum::SIGNATURE => {
                        let mut message = CharEnum::new(client_message);
                        self.client_connection.send_message(message.build_response().build_encrypted(&mut self.client_crypt)).await;
                    }
                    _ => {
                        print!("Unknown client_message: ");
                        client_message.print();
                    }
                };
            }
            _ => {}
        };
    }
}
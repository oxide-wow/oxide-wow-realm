use async_std::net::TcpStream;
use futures::executor::block_on;
use crate::message::client::auth_session::{AuthSession, Addon};
use crate::message::server::auth_challenge::AuthChallenge;
use crate::client::client::client_connection::ClientConnection;
use crate::message::client_message::ClientMessage;

pub struct ConnectingClient {
    pub client_connection: ClientConnection,
    pub client_build: u32,
    pub client_account_name: String,
    pub addons: Vec<Addon>
}

impl ConnectingClient {
    pub fn new(tcp_stream: TcpStream) -> Self {
        let mut client = Self {
            client_connection: ClientConnection::new(tcp_stream),
            client_build: 0,
            client_account_name: String::new(),
            addons: vec![]
        };
        block_on(client.client_connection.send_message(AuthChallenge::build().build()));

        client
    }

    pub async fn handle_incoming_messages(&mut self) {
        match self.client_connection.receive_message().await {
            Ok(message_raw) => {
                let mut client_message = ClientMessage::new(message_raw);
                match client_message.signature {
                    AuthSession::SIGNATURE => {
                        let message = AuthSession::new(client_message);
                        self.addons = message.addons;
                        self.client_account_name = message.account_name;
                    }
                    _ => {
                        println!("unknown package");
                        client_message.print();
                    }
                };
            }
            _ => {}
        };
    }
}
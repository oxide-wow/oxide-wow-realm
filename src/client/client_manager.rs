use std::sync::mpsc::Receiver;
use async_std::net::TcpStream;
use futures::executor::block_on;
use futures::future::join_all;
use std::thread::sleep;
use std::time::Duration;
use crate::client::client::connecting_client::ConnectingClient;
use crate::client::client::client::Client;
use crate::message::server::addon_info::AddonInfo;
use crate::message::server::auth_response::AuthResponse;

pub static THREAD_NAME: &str = "oxide_realm_client_manager";

pub struct ClientManager {
    connecting_clients: Vec<ConnectingClient>,
    clients: Vec<Client>,
    receiver: Receiver<TcpStream>
}

impl ClientManager {
    pub fn new(receiver: Receiver<TcpStream>) -> Self {
        Self {
            connecting_clients: vec![],
            clients: vec![],
            receiver
        }
    }

    pub fn run(&mut self) {
        loop {
            //remove disconnected clients
            self.connecting_clients.retain(|client| {
                client.client_connection.connected
            });
            self.clients.retain(|client| {
                client.client_connection.connected
            });

            //add new connecting clients
            match self.receiver.try_recv() {
                Ok(tcp_stream) => {
                    self.connecting_clients.push(ConnectingClient::new(tcp_stream));
                },
                Err(_) => ()
            }

            //read client messages
            let mut tasks = vec![];
            for client in &mut self.clients {
                tasks.push(client.handle_incoming_messages());
            }
            block_on(join_all(tasks));

            //move one client that authenticated from self.connecting_clients to self.clients
            match self.connecting_clients.iter().position(|client| !client.client_account_name.is_empty()) {
                Some(index) => {
                    let mut connected_client = self.connecting_clients.remove(index);
                    let mut addon_info = AddonInfo::build(&mut connected_client.addons);
                    match Client::from_connecting_client(connected_client) {
                        Ok(mut client) => {
                            // let mut message = addon_info.build_encrypted(&mut client.client_crypt);
                            // block_on(client.client_connection.send_message(message));
                            // message.append(&mut AuthResponse::build().build_encrypted(&mut client.client_crypt));
                            // let mut message = AuthResponse::build().build_encrypted(&mut client.client_crypt);
                            let mut message = AuthResponse::build().build_encrypted(&mut client.client_crypt);
                            block_on(client.client_connection.send_message(message));
                            self.clients.push(client);
                        },
                        _ => {}
                    }
                }
                _ => {}
            }

            //read connecting_client messages
            let mut tasks = vec![];
            for connecting_client in &mut self.connecting_clients {
                if connecting_client.client_account_name.is_empty() {
                    tasks.push(connecting_client.handle_incoming_messages());
                }
            }
            block_on(join_all(tasks));

            sleep(Duration::from_millis(1));
        }
    }
}
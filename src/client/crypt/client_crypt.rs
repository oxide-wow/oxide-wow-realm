use num_bigint::BigUint;
use crate::login::login_api::request_session;
use std::ops::Deref;
use sha1::Sha1;
use hmac::{Hmac, NewMac, Mac};

type HmacSha1 = Hmac<Sha1>;

pub struct ClientCrypt {
    encryption_key: Vec<u8>,
    send_i: u8,
    send_j: u8,
    recv_i: u8,
    recv_j: u8
}

impl ClientCrypt {
    pub fn new(session_key: BigUint) -> Self {
        let encryption_key = Self::build_encryption_key(&session_key);
        Self {
            encryption_key,
            send_i: 0,
            send_j: 0,
            recv_i: 0,
            recv_j: 0
        }
    }

    pub fn from_username(username: String) -> Result<ClientCrypt, ()> {
        match request_session(username) {
            Ok(session) => {
                Ok(Self::new(
                    BigUint::from_bytes_le(
                        hex::decode(session.session_key).unwrap().deref()
                    )
                ))
            }
            _ => {
                println!("api GET session from login server failed");
                Err(())
            }
        }
    }

    pub fn encrypt(&mut self, bytes: &mut Vec<u8>) {
        for t in 0..bytes.len() {
            self.send_i %= self.encryption_key.len() as u8;
            let x = (bytes[t] ^ self.encryption_key[self.send_i as usize]).wrapping_add(self.send_j);
            self.send_i += 1;
            self.send_j = x;
            bytes[t] = x;
        }
    }

    pub fn decrypt_6_bytes(&mut self, bytes: &mut Vec<u8>) {
        for t in 0..6 {
            self.recv_i %= self.encryption_key.len() as u8;
            let x = bytes[t].wrapping_sub(self.recv_j) ^ self.encryption_key[self.recv_i as usize];
            self.recv_i += 1;
            self.recv_j = bytes[t];
            bytes[t] = x;
        }
    }

    fn build_encryption_key(session_key: &BigUint) -> Vec<u8> {
        let seed = vec![56, 167, 131, 21, 248, 146, 37, 48, 113, 152, 103, 177, 140, 4, 226, 170];
        let mut hmac = HmacSha1::new_from_slice(&seed).expect("hmac init fail");
        hmac.update(session_key.to_bytes_be().as_slice());
        hmac.finalize().into_bytes().to_vec()
    }
}

#[test]
fn test() {
    let session_key = BigUint::from_bytes_le(
        hex::decode("7076740013e2293cbe2bcf55e0968a49bf2406c9f2f902ff1993acb82e59a868").unwrap().deref()
    );

    assert_eq!("f9574dd01356df41342d0b2e9b655b93bf592a42", hex::encode(ClientCrypt::build_encryption_key(&session_key)));

    let mut client_crypt = ClientCrypt::new(session_key);
    let mut bytes = vec![0, 205, 239, 2];
    client_crypt.encrypt(&mut bytes);
    assert_eq!(vec![249, 147, 53, 7], bytes);

    let mut client_bytes = vec![206, 0, 0, 0, 243, 2];
    client_crypt.decrypt_6_bytes(&mut client_bytes);
    assert_eq!(vec![55, 101, 77, 208, 224, 89], client_bytes);
}
use std::sync::mpsc::{Sender, channel};
use crate::client::client_manager::{ClientManager, THREAD_NAME};
use async_std::net::{TcpListener, TcpStream};
use futures::StreamExt;
use futures::executor::block_on;

pub struct ClientGateway {
    sender: Sender<TcpStream>
}

impl ClientGateway {
    pub fn run() {
        let (sender, receiver) = channel();

        std::thread::Builder::new().name(String::from(THREAD_NAME)).spawn(move || {
            let mut client_manager = ClientManager::new(receiver);
            client_manager.run();
        }).unwrap();

        let mut client_gateway = Self::new(sender);
        block_on(client_gateway.listen()).unwrap();
    }

    fn new(sender: Sender<TcpStream>) -> Self {
        Self {
            sender
        }
    }

    async fn listen(&mut self) -> std::io::Result<()> {
        let listener = TcpListener::bind("0.0.0.0:8085").await.unwrap();
        let port = listener.local_addr()?;
        println!("Listening on {}...", port);

        let sender = &self.sender;
        listener.incoming().for_each_concurrent(None, |tcp_stream| async move {
            let tcp_stream = tcp_stream.unwrap();
            let addr = tcp_stream.peer_addr().unwrap();
            println!("Client connected: {:?}", addr);
            sender.send(tcp_stream).unwrap();
        }).await;

        Ok(())
    }
}
use client::client_gateway::ClientGateway;

mod client;
mod login;
mod message;

fn main() {
    ClientGateway::run();
}
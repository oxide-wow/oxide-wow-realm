use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize)]
pub struct Session {
    pub session_key: String
}

pub fn request_session(username: String) -> Result<Session, reqwest::Error> {
    let url = format!("http://127.0.0.1:8001/session/{}", username);
    reqwest::blocking::get(url)?.json()
}
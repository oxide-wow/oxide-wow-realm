use std::io::{Cursor, Read};
use byteorder::{WriteBytesExt, LittleEndian, BigEndian};
use crate::client::crypt::client_crypt::ClientCrypt;

pub struct ServerMessage {
    pub signature: u16,
    pub payload: Vec<u8>
}

impl ServerMessage {
    pub fn new(signature: u16) -> Self {
        Self {
            signature,
            payload: vec![]
        }
    }

    pub fn build(&mut self) -> Vec<u8> {
        let mut message = self.build_header();
        message.append(&mut self.payload);
        message
    }

    pub fn build_encrypted(&mut self, client_crypt: &mut ClientCrypt) -> Vec<u8> {
        let mut message = self.build_header();
        client_crypt.encrypt(&mut message);
        message.append(&mut self.payload);
        message
    }

    fn build_header(&mut self) -> Vec<u8> {
        let mut message = vec![];
        message.append(&mut Self::u16_to_be(self.payload.len() as u16 + 2));
        message.append(&mut Self::u16_to_le(self.signature));
        message
    }

    pub fn add_u8(&mut self, value: u8) {
        self.payload.push(value);
    }

    pub fn add_u16(&mut self, value: u16) {
        self.payload.append(Self::u16_to_le(value).as_mut());
    }

    pub fn add_u32(&mut self, value: u32) {
        self.payload.append(Self::u32_to_le(value).as_mut());
    }

    pub fn add_vec_u8(&mut self, value: Vec<u8>) {
        let mut v = value;
        self.payload.append(v.as_mut());
    }

    pub fn add_string(&mut self, value: String) {
        self.add_vec_u8(value.into_bytes());
    }

    pub fn read_string(&mut self, byte_index: u64) -> String {
        let mut c = Cursor::new(&self.payload);
        c.set_position(byte_index);
        let mut result = String::new();
        let _ = c.read_to_string(&mut result);
        result
    }

    pub fn u16_to_le(value: u16) -> Vec<u8> {
        let mut append = vec![];
        let _ = append.write_u16::<LittleEndian>(value);
        append
    }

    pub fn u16_to_be(value: u16) -> Vec<u8> {
        let mut append = vec![];
        let _ = append.write_u16::<BigEndian>(value);
        append
    }

    pub fn u32_to_le(value: u32) -> Vec<u8> {
        let mut append = vec![];
        let _ = append.write_u32::<LittleEndian>(value);
        append
    }

    pub fn u32_to_be(value: u32) -> Vec<u8> {
        let mut append = vec![];
        let _ = append.write_u32::<BigEndian>(value);
        append
    }

    pub fn f32_to_le(value: f32) -> Vec<u8> {
        let mut append = vec![];
        let _ = append.write_f32::<LittleEndian>(value);
        append
    }
}

#[test]
fn test() {
    let mut msg = ServerMessage::new(555);
    msg.add_u8(1);
    msg.add_u16(0xaabb);
    msg.add_u32(0xaabbccdd);
    msg.add_vec_u8(vec![2, 4, 6, 8]);
    msg.add_string(String::from("test"));
    msg.add_u8(9);

    assert_eq!(555, msg.signature);
    assert_eq!(vec![1, 187, 170, 221, 204, 187, 170, 2, 4, 6, 8, 116, 101, 115, 116, 9], msg.payload);
    assert_eq!(vec![0, 18, 43, 2, 1, 187, 170, 221, 204, 187, 170, 2, 4, 6, 8, 116, 101, 115, 116, 9], msg.build());
}
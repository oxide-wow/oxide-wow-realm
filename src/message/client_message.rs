use std::io::{Cursor, BufRead, Read};
use byteorder::{ReadBytesExt, LittleEndian, BigEndian};
use crate::message::server_message::ServerMessage;

pub struct ClientMessage {
    pub length: u16,
    pub signature: u32,
    pub payload: Vec<u8>
}

impl ClientMessage {
    pub fn new(raw: Vec<u8>) -> Self {
        let mut cursor = Cursor::new(&raw);
        let length = cursor.read_u16::<BigEndian>().unwrap();
        cursor.set_position(2);
        let signature = cursor.read_u32::<LittleEndian>().unwrap();
        cursor.set_position(6);
        let mut payload = vec![];
        let _ = cursor.read_to_end(&mut payload);
        Self {
            length,
            signature,
            payload
        }
    }

    pub fn read_u8(&mut self, byte_index: u64) -> u8 {
        let mut c = Cursor::new(&self.payload);
        c.set_position(byte_index);
        c.read_u8().unwrap()
    }

    pub fn read_u16(&mut self, byte_index: u64) -> u16 {
        let mut c = Cursor::new(&self.payload);
        c.set_position(byte_index);
        c.read_u16::<LittleEndian>().unwrap()
    }

    pub fn read_u32(&mut self, byte_index: u64) -> u32 {
        let mut c = Cursor::new(&self.payload);
        c.set_position(byte_index);
        c.read_u32::<LittleEndian>().unwrap()
    }

    pub fn read_vec_u8_20(&mut self, byte_index: u64) -> Vec<u8> {
        let mut c = Cursor::new(&self.payload);
        c.set_position(byte_index);
        let mut result: [u8;20] = [0;20];
        c.read(result.as_mut()).unwrap();
        Vec::from(result)
    }

    pub fn read_vec_u8_32(&mut self, byte_index: u64) -> Vec<u8> {
        let mut c = Cursor::new(&self.payload);
        c.set_position(byte_index);
        let mut result: [u8;32] = [0;32];
        c.read(result.as_mut()).unwrap();
        Vec::from(result)
    }

    pub fn read_vec_u8_to_end(&mut self, byte_index: u64) -> Vec<u8> {
        let mut c = Cursor::new(&self.payload);
        c.set_position(byte_index);
        let mut result = vec![];
        c.read_to_end(&mut result).unwrap();
        result
    }

    pub fn read_string(&mut self, byte_index: u64) -> String {
        let mut c = Cursor::new(&self.payload);
        c.set_position(byte_index);
        let mut buffer = vec![];
        let _ = c.read_until(0, &mut buffer);
        match buffer.last() {
            Some(byte) => {
                if *byte == 0 {
                    buffer.pop();
                }
            }
            _ => {}
        }

        String::from_utf8(buffer).unwrap()
    }

    pub fn print(&mut self) {
        println!("Signature: {} Payload hex: {} Message dump: {:?}", hex::encode(ServerMessage::u32_to_be(self.signature)), hex::encode(&self.payload), self.payload);
    }
}

#[test]
fn test() {
    let msg = ClientMessage::new(vec![1, 2, 3, 4, 5, 6, 7, 8, 9]);
    assert_eq!(258, msg.length);
    assert_eq!(100992003, msg.signature);
    assert_eq!(vec![7, 8, 9], msg.payload);

    let mut msg = ClientMessage::new(vec![1, 2, 3, 4, 5, 6, 80, 76, 65, 89, 69, 82, 0, 14]);
    let name = msg.read_string(4);
    assert_eq!("ER", name);

    let mut msg = ClientMessage::new(vec![1, 2, 3, 4, 5, 6, 80, 76, 65, 89, 69, 82]);
    let name = msg.read_string(0);
    assert_eq!("PLAYER", name);
}
use crate::message::server_message::ServerMessage;

enum AuthResult {
    Ok = 0x0c,
    Failed = 0x0d,
    Banned = 0x1c
}

// sent after addon_info
pub struct AuthResponse {}

impl AuthResponse {
    pub const SIGNATURE: u16 = 0x1ee;

    pub fn build() -> ServerMessage {
        let mut server_message = ServerMessage::new(Self::SIGNATURE);
        server_message.add_u8(AuthResult::Ok as u8);
        server_message.add_u32(0);
        server_message.add_u8(0);
        server_message.add_u32(0);
        server_message.add_u8(1);

        server_message
    }
}
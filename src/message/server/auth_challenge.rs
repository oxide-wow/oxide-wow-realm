use rand::rngs::OsRng;
use rand::RngCore;
use crate::message::server_message::ServerMessage;

// first message to connecting clients
pub struct AuthChallenge {}

impl AuthChallenge {
    pub const SIGNATURE: u16 = 0x1ec;

    pub fn build() -> ServerMessage {
        let mut server_message = ServerMessage::new(Self::SIGNATURE);
        server_message.add_u32(0);
        server_message.add_vec_u8(Self::generate_encryption_seed());
        server_message.add_vec_u8(Self::generate_encryption_seed());

        server_message
    }

    fn generate_encryption_seed() -> Vec<u8> {
        let mut b = [0u8; 16];
        OsRng.fill_bytes(&mut b);
        Vec::from(b)
    }
}
use crate::message::client_message::ClientMessage;
use crate::message::server_message::ServerMessage;

//
pub struct CharEnum {
    pub client_message: ClientMessage
}

impl CharEnum {
    pub const SIGNATURE: u32 = 0x037;
    pub const RESPONSE_SIGNATURE: u16 = 0x03b;

    pub fn new(mut client_message: ClientMessage) -> Self {
        Self {
            client_message
        }
    }

    pub fn build_response(&mut self) -> ServerMessage {
        let mut server_message = ServerMessage::new(Self::RESPONSE_SIGNATURE);
        // server_message.add_u8(0);
        // server_message.add_u32(1);
        //
        // let character = Character::new();
        // server_message.add_vec_u8(character.to_enum_vec());
        //
        // server_message.add_u8(0);
        server_message.add_vec_u8(hex::decode("ffffffff0000000030312f30312f303100a51b7eff0102000000000000005465737400010100060a050304010c00000000000000cdb90bc66dc703c3be50a742000000000000000200000000000000000000000000000000000000000000000000000000000000000000000000000000a32600000400000000000000000000000000000000000000000000a426000007000000009d27000008000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000606000015000000002a4900000e00000000000000000000000000000000000000000000000000000000000000").unwrap());

        server_message
    }
}

pub struct Character {
    name: String
}

impl Character {
    pub fn new() -> Self {
        Self {
            name: "Test".to_string()
        }
    }

    pub fn to_enum_vec(&self) -> Vec<u8> {
        let mut result = vec![];

        result.append(&mut vec![0xff, 0xff, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x30, 0x31, 0x2f, 0x30, 0x31, 0x2f, 0x30, 0x31, 0x00, 0xa5, 0x1b, 0x7e, 0xff, 0x01]);

        result.append(&mut vec![1, 0, 0, 0, 0, 0, 0, 0]);

        result.append(self.name.clone().into_bytes().as_mut());
        result.push(0);

        result.push(1);
        result.push(1);
        result.push(0);

        result.push(6);
        result.push(10);
        result.push(5);
        result.push(3);
        result.push(4);

        result.push(19);
        result.append(&mut vec![0x0c, 0, 0, 0]);
        result.append(&mut vec![0, 0, 0, 0]);

        result.append(&mut ServerMessage::f32_to_le(-8942.45));
        result.append(&mut ServerMessage::f32_to_le(-131.779));
        result.append(&mut ServerMessage::f32_to_le(83.6577));

        result.append(&mut vec![0, 0, 0, 1]);
        result.append(&mut vec![0, 0, 0, 0]);

        result.push(0);

        result.append(&mut vec![0, 0, 0, 0]);
        result.append(&mut vec![0, 0, 0, 0]);
        result.append(&mut vec![0, 0, 0, 0]);

        result
    }
}

#[test]
fn test() {

}

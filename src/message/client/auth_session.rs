use crate::message::client_message::ClientMessage;
use flate2::read;
use std::io::{Read, Cursor, BufRead};
use byteorder::{LittleEndian, ReadBytesExt};

// sent after AuthChallenge
pub struct AuthSession {
    pub client_message: ClientMessage,
    pub account_name: String,
    pub addons: Vec<Addon>
}

impl AuthSession {
    pub const SIGNATURE: u32 = 0x1ed;

    pub fn new(mut client_message: ClientMessage) -> Self {
        let account_name = client_message.read_string(8);
        let addon_data_raw = client_message.read_vec_u8_to_end(43);
        let mut zlib_decoder = read::ZlibDecoder::new(&addon_data_raw[..]);
        let mut addon_data = vec![];
        zlib_decoder.read_to_end(&mut addon_data).unwrap();
        // let account_len = account_name.len() as u64;
        // let client_seed = message_reader.read_u32(15 + account_len);
        // let checksum = message_reader.read_vec_u8_20(19 + account_len);

        let mut addons = vec![];
        while !addon_data.is_empty() {
            addons.push(Addon::new(&mut addon_data));
        }

        Self {
            client_message,
            account_name,
            addons
        }
    }
}

pub struct Addon {
    pub name: String,
    pub crc: u32
}

impl Addon {
    pub fn new(addon_data: &mut Vec<u8>) -> Self {
        let mut c = Cursor::new(&addon_data);
        let mut buffer = vec![];
        let _ = c.read_until(0, &mut buffer);
        match buffer.last() {
            Some(byte) => {
                if *byte == 0 {
                    buffer.pop();
                }
            }
            _ => {}
        }
        let name = String::from_utf8(buffer).unwrap();

        c.set_position(name.len() as u64 + 1);
        let crc = c.read_u32::<LittleEndian>().unwrap();

        addon_data.drain(..name.len() + 10);

        Self {
            name,
            crc
        }
    }

    pub fn is_blizzard_addon(&self) -> bool {
        self.crc == 0x1c776d01
    }
}

#[test]
fn test() {
    let mut addon_data = vec![66, 108, 105, 122, 122, 97, 114, 100, 95, 65, 117, 99, 116, 105, 111, 110, 85, 73, 0, 1, 109, 119, 28, 76, 0, 0, 0, 0, 66, 108, 105, 122, 122, 97, 114, 100, 95, 66, 97, 116, 116, 108, 101, 102, 105, 101, 108, 100, 77, 105, 110, 105, 109, 97, 112, 0, 1, 109, 119, 28, 76, 0, 0, 0, 0, 66, 108, 105, 122, 122, 97, 114, 100, 95, 66, 105, 110, 100, 105, 110, 103, 85, 73, 0, 1, 109, 119, 28, 76, 0, 0, 0, 0, 66, 108, 105, 122, 122, 97, 114, 100, 95, 67, 111, 109, 98, 97, 116, 76, 111, 103, 0, 1, 109, 119, 28, 76, 0, 0, 0, 0, 66, 108, 105, 122, 122, 97, 114, 100, 95, 67, 111, 109, 98, 97, 116, 84, 101, 120, 116, 0, 1, 109, 119, 28, 76, 0, 0, 0, 0, 66, 108, 105, 122, 122, 97, 114, 100, 95, 67, 114, 97, 102, 116, 85, 73, 0, 1, 109, 119, 28, 76, 0, 0, 0, 0, 66, 108, 105, 122, 122, 97, 114, 100, 95, 71, 77, 83, 117, 114, 118, 101, 121, 85, 73, 0, 1, 109, 119, 28, 76, 0, 0, 0, 0, 66, 108, 105, 122, 122, 97, 114, 100, 95, 71, 117, 105, 108, 100, 66, 97, 110, 107, 85, 73, 0, 1, 109, 119, 28, 76, 0, 0, 0, 0, 66, 108, 105, 122, 122, 97, 114, 100, 95, 73, 110, 115, 112, 101, 99, 116, 85, 73, 0, 1, 109, 119, 28, 76, 0, 0, 0, 0, 66, 108, 105, 122, 122, 97, 114, 100, 95, 73, 116, 101, 109, 83, 111, 99, 107, 101, 116, 105, 110, 103, 85, 73, 0, 1, 109, 119, 28, 76, 0, 0, 0, 0, 66, 108, 105, 122, 122, 97, 114, 100, 95, 77, 97, 99, 114, 111, 85, 73, 0, 1, 109, 119, 28, 76, 0, 0, 0, 0, 66, 108, 105, 122, 122, 97, 114, 100, 95, 82, 97, 105, 100, 85, 73, 0, 1, 109, 119, 28, 76, 0, 0, 0, 0, 66, 108, 105, 122, 122, 97, 114, 100, 95, 84, 97, 108, 101, 110, 116, 85, 73, 0, 1, 109, 119, 28, 76, 0, 0, 0, 0, 66, 108, 105, 122, 122, 97, 114, 100, 95, 84, 114, 97, 100, 101, 83, 107, 105, 108, 108, 85, 73, 0, 1, 109, 119, 28, 76, 0, 0, 0, 0, 66, 108, 105, 122, 122, 97, 114, 100, 95, 84, 114, 97, 105, 110, 101, 114, 85, 73, 0, 1, 109, 119, 28, 76, 0, 0, 0, 0, 66, 108, 105, 122, 122, 97, 114, 100, 95, 84, 105, 109, 101, 77, 97, 110, 97, 103, 101, 114, 0, 1, 109, 119, 28, 76, 0, 0, 0, 0];
    let mut addons = vec![];
    while !addon_data.is_empty() {
        addons.push(Addon::new(&mut addon_data));
    }
    for addon in &addons {
        assert_eq!(true, addon.is_blizzard_addon());
    }
    assert_eq!("Blizzard_AuctionUI", addons[0].name);
    assert_eq!("Blizzard_BattlefieldMinimap", addons[1].name);
    assert_eq!("Blizzard_BindingUI", addons[2].name);
    assert_eq!("Blizzard_CombatLog", addons[3].name);
    assert_eq!("Blizzard_CombatText", addons[4].name);
    assert_eq!("Blizzard_CraftUI", addons[5].name);
    assert_eq!("Blizzard_GMSurveyUI", addons[6].name);
    assert_eq!("Blizzard_GuildBankUI", addons[7].name);
    assert_eq!("Blizzard_InspectUI", addons[8].name);
    assert_eq!("Blizzard_ItemSocketingUI", addons[9].name);
    assert_eq!("Blizzard_MacroUI", addons[10].name);
    assert_eq!("Blizzard_RaidUI", addons[11].name);
    assert_eq!("Blizzard_TalentUI", addons[12].name);
    assert_eq!("Blizzard_TradeSkillUI", addons[13].name);
    assert_eq!("Blizzard_TrainerUI", addons[14].name);
    assert_eq!("Blizzard_TimeManager", addons[15].name);
}